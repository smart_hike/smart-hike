#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"


static WORKING_AREA(waThread1, 128);

static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
   
  }
  return(0);
}


//----------- Enviar comando ---------
void Sendcmd(unsigned char address,unsigned char cmd)
{ 
  chIOPut((BaseChannel *)&SD2,address);//set the address of SRF02(factory default is 0)
  chThdSleepMicroseconds(100);  
  chIOPut((BaseChannel *)&SD2,cmd); //send the command to SRF02
  chThdSleepMicroseconds(100); 
}

//----------- Encender motor ---------
void motor(int time)
{
	palSetPad(IOPORT1, PIOA_SP_M0); //En 1
	palClearPad(IOPORT1, PIOA_DIR_M0); // En 0

	chThdSleepMilliseconds(time);  //vibra X ms

	palClearPad(IOPORT1, PIOA_SP_M0); //En 0 . apaga motor

}







//**************************      MAIN      *********************** 
void main (void)
 {
  halInit();
  chSysInit();

  sdStart(&SD2, NULL);  // Activates the serial driver 
  sdStart(&SD1, NULL);


//----------- Configuración pines ------------

  palSetPadMode(IOPORT1, PIOA_DIG10, PAL_MODE_INPUT_PULLUP); //sw encendido
  palSetPadMode(IOPORT1, PIOA_DIG8, PAL_MODE_OUTPUT_PUSHPULL); //Fuente 3.3
  
  palSetPadMode(IOPORT1, PIOA_SP_M0, PAL_MODE_OUTPUT_PUSHPULL); // motor
  palSetPadMode(IOPORT1, PIOA_DIR_M0, PAL_MODE_OUTPUT_PUSHPULL); // motor


//----------- Declaración de variables --------- 
 unsigned char dis_alto, dis_bajo;
 unsigned int sw_on=0; 

 int i=0; 

 chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

 Sendcmd(0x00,0x60);//comando calibrar sensor bajo
 Sendcmd(0x02,0x60);//comando calibrar sensor alto

 palSetPad(IOPORT1, PIOA_DIG8); //encender fuente 3.3

 while(1){

	sw_on = palReadPad(IOPORT1, PIOA_DIG10); //leer sw encendido
	
	if(sw_on==1){


		if (i==0){
			//chprintf((BaseChannel *)&SD1, "%d; ", 1);	// Reproducir audio	
			palSetPad(IOPORT2, PIOB_LED2); //encender Led
			motor(500);	// señal de encendido 	
			i++;
		}
	
		Sendcmd(0x00,0x51);//comando medición sensor bajo
		chThdSleepMilliseconds(66);
		Sendcmd(0x00,0x5E); //comando leer sensor bajo
		chThdSleepMilliseconds(10);   
	 
		dis_bajo  = chIOGet(&SD2)<<8; //receive high byte (overwrites previous reading) and shift high byte to be high 8 bits
        	dis_bajo |= chIOGet(&SD2); 	
		
		chprintf((BaseChannel *)&SD1, "%d; ", dis_bajo);

		Sendcmd(0x02,0x51);//comando medición sensor alto
		chThdSleepMilliseconds(66);
		Sendcmd(0x02,0x5E); //comando leer sensor alto
		chThdSleepMilliseconds(10);   

		dis_alto  = chIOGet(&SD2)<<8; //receive high byte (overwrites previous reading) and shift high byte to be high 8 bits
        	dis_alto |= chIOGet(&SD2); 	

	       	if(dis_alto<=0x64){// vibrar si obstaculo está a 1m del sensor alto  
				motor(1000); 
		}



 	}

	else { //cuando se apaga el sw
		palClearPad(IOPORT2, PIOB_LED2); //apagar Led
		i=0;
	}

 }

}
